// ConsoleApplication1.cpp: ���������� ����� ����� ��� ����������� ����������.
//


#include "stdafx.h"
#include <Windows.h>
#include <tchar.h>
#include <stdio.h> 

#define WC(f) if (!(f)) { printf("Failed: Error=%x\n",GetLastError()); abort();}

BOOL IsWow64()
{
	BOOL bIsWow64 = FALSE;
	typedef BOOL(WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
	LPFN_ISWOW64PROCESS fnIsWow64Process;

	//IsWow64Process is not available on all supported versions of Windows.
	//Use GetModuleHandle to get a handle to the DLL that contains the function
	//and GetProcAddress to get a pointer to the function if available.

	fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(
		GetModuleHandle(TEXT("kernel32")), "IsWow64Process");

	if (NULL != fnIsWow64Process)
	{
		if (!fnIsWow64Process(GetCurrentProcess(), &bIsWow64))
		{
			//handle error
		}
	}
	return bIsWow64;
}


inline unsigned char make_byte(const void * ptr, unsigned n)
{
	return (((uintptr_t)ptr) >> (n << 3)) & 0xFF;
}
#define MAKE_QWORD(ptr) make_byte(ptr,0),make_byte(ptr,1),make_byte(ptr,2),make_byte(ptr,3),make_byte(ptr,4),\
		make_byte(ptr,5),make_byte(ptr,6),make_byte(ptr,7)
#define MAKE_DWORD(ptr) make_byte(ptr,0),make_byte(ptr,1),make_byte(ptr,2),make_byte(ptr,3)

void createShellcode(unsigned char** shellcode, size_t * shellcodeSize, void * load_lib_addr, void * dll_str_addr)
{
	unsigned char sc[] = {
#if 1//64bit variant
		//push rbp
		0x55,
		//mov rbp, rsp
		0x48, 0x89, 0xE5,
		//sub rsp, 32(0x20) (or 40(0x28))
		0x48, 0x83, 0xEC, 0x20,
		//mov rcx, dll_str_addr
		0x48, 0xb9, MAKE_QWORD(dll_str_addr),
		//mov rax, load_lib_addr
		0x48, 0xb8, MAKE_QWORD(load_lib_addr),
		//call [rax]
		0xff, 0xd0,
		//nop
		0x90,
		//add rsp, 32
		0x48, 0x83, 0xC4, 0x20,
		//pop rbp
		0x5d,

		//ret
		0xc3
#else//32 bit variant
		// Push all flags
		0x9C,
		// Push all register
		0x60,
		//0xb9, MAKE_QWORD_32(dll_str_addr),
		0x68, MAKE_DWORD(dll_str_addr),
		0xb8, MAKE_DWORD(load_lib_addr),
		// Call eax
		//0xff, 0x10,
		0xFF, 0xD0,
		// Pop all register
		0x61,
		// Pop all flags
		0x9D,
		0xC3
#endif

	};

	size_t len = sizeof(sc);
	*shellcodeSize = len;
	*shellcode = (unsigned char*)malloc(len);
	memcpy(*shellcode, sc, len);

}


short DllMain(LPCTSTR artist);
short AddArtist(LPCTSTR artist, LPCTSTR country, short century);
short FindArtifact(LPCTSTR name, LPCTSTR artist);
short AddArtifact(LPCTSTR artist, LPCTSTR name, short price);


int _tmain(int argc, char* argv[])
{
	unsigned char* shellcode;
	size_t shellcodeLen;

	char dllPath[] = "D:\\Sys_prog\\3_task\\Work\\Work\\Send\\TestDLL64\\x64\\Debug\\TestDLL64.dll";//for 64 bit 
	//char dllPath[] = "D:\\Sys_prog\\3_task\\Work\\Work\\Send\\TestDLL32\\Debug\\TestDLL32.dll";//for 32 bit 

	LPVOID remote_shellcodePtr;
	LPVOID remote_dllStringPtr;
	LPVOID remote_loadlib_addr;

	// Create Process SUSPENDED
	PROCESS_INFORMATION pi;
	STARTUPINFOA Startup;
	ZeroMemory(&Startup, sizeof(Startup));
	ZeroMemory(&pi, sizeof(pi));
	//WC(CreateProcessA("C:\\Windows\\notepad.exe", NULL, NULL, NULL, NULL, CREATE_SUSPENDED, NULL, NULL, &Startup, &pi));
	//ResumeThread(pi.hThread);
	//Sleep(1000);
	//SuspendThread(pi.hThread);
	int ID;
	puts("Inject into which PID?");
	scanf_s("%u", &ID);
	if (ID == 0) {
		ID = GetCurrentProcessId();
	}
	pi.hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, ID);
	if (pi.hProcess == NULL)
	{
		//some error
		printf("Can't open such process\n");
		return 0;
	}

	printf("Allocating Remote Memory For DLL Path\n");
	remote_dllStringPtr = VirtualAllocEx(pi.hProcess, NULL, strlen(dllPath) + 1, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if (remote_dllStringPtr == NULL) {
		printf("Error: the memory could not be allocated inside the chosen process.\n");
		getchar();
		return -1;
	}

	printf("DLL Adress: %p\n", remote_dllStringPtr);

	//assume that this process and the process we are going to inject have the same address of LoadLibrary
	remote_loadlib_addr = GetProcAddress(LoadLibraryA("kernel32.dll"), "LoadLibraryA");
	printf("LoadLibraryA Adress: %p\n", remote_loadlib_addr);

	printf("Build Shellcode\n");
	createShellcode(&shellcode, &shellcodeLen, remote_loadlib_addr, remote_dllStringPtr);

	printf("Created Shellcode: \n");
	for (int i = 0; i<shellcodeLen; i++)
		printf("%X ", shellcode[i]);
	printf("\n");

	printf("Allocating Remote Memory For Shellcode\n");
	remote_shellcodePtr = (LPVOID)VirtualAllocEx(pi.hProcess, NULL, shellcodeLen, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE); //firstly was PAGE_EXECUTE_READWRITE
	if (remote_shellcodePtr == NULL) {
		printf("Error: the memory could not be allocated inside the chosen process.\n");
		getchar();
		return -1;
	}

	printf("Shellcode Adress: %p\n", remote_shellcodePtr);



	printf("Write DLL Path To Remote Process\n");
	int n = WriteProcessMemory(pi.hProcess, remote_dllStringPtr, dllPath, strlen(dllPath) + 1, NULL);
	if (n == 0) {
		printf("Error: there was no bytes written to the process's address space.\n");
		getchar();
		return -1;
	}

	printf("Write Shellcode To Remote Process\n");
	n = WriteProcessMemory(pi.hProcess, remote_shellcodePtr, shellcode, shellcodeLen, NULL);
	if (n == 0) {
		printf("Error: there was no bytes written to the process's address space.\n");
		getchar();
		return -1;
	}

	printf("Make shellcode PAGE_EXECUTE_READ\n");

	DWORD oldProtect = NULL;
	WC(VirtualProtectEx(pi.hProcess, remote_dllStringPtr, strlen(dllPath) + 1, PAGE_EXECUTE_READ, &oldProtect));
	WC(VirtualProtectEx(pi.hProcess, remote_shellcodePtr, shellcodeLen, PAGE_EXECUTE_READ, &oldProtect));


	printf("CreateRemoteThread\n");
	HANDLE hThreadRemote = CreateRemoteThread(pi.hProcess, NULL, 0,
		(PTHREAD_START_ROUTINE)remote_shellcodePtr,//lpStartAddress
		NULL,//lpParameter,
		0,
		NULL);
	if (hThreadRemote == NULL) {
		printf("Error: the remote thread could not be created, error code is: %d.\n", GetLastError());//GetLastError() = 5 => Access is denied. 
		getchar();
		return -1;
	}
	else {
		printf("Success: the remote thread was successfully created.\n");
	}

	printf("Starting thread\n");

	printf("Wait Till Code Was Executed\n");
	//Sleep(8000);

	printf("wait for remote thread\n");
	//wait for remote thread

	//0 is success
	DWORD res = WaitForSingleObject(hThreadRemote, INFINITE);
	printf("WAIT: Error=%x\n", res);

	printf("Free Remote Resources\n");
	VirtualFreeEx(pi.hProcess, remote_dllStringPtr, shellcodeLen, MEM_DECOMMIT);
	VirtualFreeEx(pi.hProcess, remote_shellcodePtr, shellcodeLen, MEM_DECOMMIT);
	if (hThreadRemote != NULL)
		CloseHandle(hThreadRemote);

	// Close process and thread handles. 
	CloseHandle(pi.hProcess);
	//CloseHandle(pi.hThread);
	getchar();
	return 0;
}




