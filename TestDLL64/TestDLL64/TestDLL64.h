// ����������� ���� ���� ifdef - ��� ����������� ����� �������� ��������, ���������� ��������� 
// �������� �� ��������� DLL. ��� ����� ������ DLL �������������� � �������������� ������� TESTDLL64_EXPORTS,
// � ��������� ������. ���� ������ �� ������ ���� ��������� � �����-���� �������
// ������������ ������ DLL. ��������� ����� ����� ������ ������, ��� �������� ����� �������� ������ ����, ����� 
// ������� TESTDLL64_API ��� ��������������� �� DLL, ����� ��� ������ DLL ����� �������,
// ������������ ������ ��������, ��� ����������������.
#ifdef TESTDLL64_EXPORTS
#define TESTDLL64_API __declspec(dllexport)
#else
#define TESTDLL64_API __declspec(dllimport)
#endif

// ���� ����� ������������� �� TestDLL64.dll
class TESTDLL64_API CTestDLL64 {
public:
	CTestDLL64(void);
	// TODO: �������� ����� ���� ������.
};

extern TESTDLL64_API int nTestDLL64;

TESTDLL64_API int fnTestDLL64(void);
