// ����������� ���� ���� ifdef - ��� ����������� ����� �������� ��������, ���������� ��������� 
// �������� �� ��������� DLL. ��� ����� ������ DLL �������������� � �������������� ������� TESTDLL32_EXPORTS,
// � ��������� ������. ���� ������ �� ������ ���� ��������� � �����-���� �������
// ������������ ������ DLL. ��������� ����� ����� ������ ������, ��� �������� ����� �������� ������ ����, ����� 
// ������� TESTDLL32_API ��� ��������������� �� DLL, ����� ��� ������ DLL ����� �������,
// ������������ ������ ��������, ��� ����������������.
#ifdef TESTDLL32_EXPORTS
#define TESTDLL32_API __declspec(dllexport)
#else
#define TESTDLL32_API __declspec(dllimport)
#endif

// ���� ����� ������������� �� TestDLL32.dll
class TESTDLL32_API CTestDLL32 {
public:
	CTestDLL32(void);
	// TODO: �������� ����� ���� ������.
};

extern TESTDLL32_API int nTestDLL32;

TESTDLL32_API int fnTestDLL32(void);
